import './App.css'
import Roars from './components/Roars'
import Visit from './components/Visit'
import Footer from './components/footer'
import Header from './components/header'

function App() {
  return (
    <>
  <Header/>
  <main>
    <section>
      <div className="container">
        <h1>The Legacy of Tigers in the Wild</h1>
        <p>
          Welcome to Tigerscape, where nature's magnificence meets the raw power
          of the jungle's rulers. Nestled within lush wilderness, Tigerscape
          Reserve offers a sanctuary for these majestic creatures. Traverse
          through dense forests, breathe in the untamed air, and witness the
          awe-inspiring beauty of tigers roaming freely. Embark on an
          unforgettable journey into the heart of Tigerscape.
        </p>
        <div className="buttons">
          <a href="#" className="button button-primary">
            Book Your Safari{" "}
            <span className="material-symbols-outlined">arrow_forward</span>
          </a>
          <a href="#" className="button button-secondary">
            Contact Us
          </a>
        </div>
      </div>
    </section>
    <section id="section1">
      <div className="container">
        <h2>Our Mission: Protecting Tigers and Preserving Their Habitat</h2>
        <p>
          TigerScape stands as a beacon of hope for the preservation of tigers
          and their habitat. Our unwavering commitment to conservation drives us
          to implement innovative strategies, advocate for wildlife protection,
          and engage communities in sustainable practices. Through research,
          education, and on-the-ground initiatives, we strive to secure a future
          where tigers roam freely and ecosystems flourish. Join us in
          safeguarding the legacy of these majestic creatures for generations to
          come.
        </p>
      </div>
    </section>
    <Roars/>
    <Visit/>
  </main>
    <Footer/>
</>

  )
}

export default App
