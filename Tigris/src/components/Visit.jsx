function Visit(){
    return (
        <section className="section3">
        <div className="container">
          <h2>Plan Your Visit</h2>
          <div className="VisitList" id="visitList">
            <article>
              <div className="visitArticle">
                <img
                  className="imageVisit"
                  src="src/assets/images/tenthouse.jpg"
                  alt="tent house"
                />
                <div className="viewList">
                  <h4>Tented House</h4>
                  <a href="#">View Details</a>
                </div>
              </div>
            </article>
            <article>
              <div className="visitArticle">
                <img
                  className="imageVisit"
                  src="src/assets/images/elephant.jpg"
                  alt="elephant"
                />
                <div className="viewList">
                  <h4>Trekking Trail</h4>
                  <a href="#">View Details</a>
                </div>
              </div>
            </article>
            <article>
              <div className="visitArticle">
                <img
                  className="imageVisit"
                  src="src/assets/images/treetophut.jpg"
                  alt="treehut house"
                />
                <div className="viewList">
                  <h4>Treetop Hut</h4>
                  <a href="#">View Details</a>
                </div>
              </div>
            </article>
            <article>
              <div className="visitArticle">
                <img
                  className="imageVisit"
                  src="src/assets/images/tiger1.jpg"
                  alt="tiger"
                />
                <div className="viewList">
                  <h4>Mark Trekking</h4>
                  <a href="#">View Details</a>
                </div>
              </div>
            </article>
          </div>
        </div>
      </section>
    )
}
export default Visit