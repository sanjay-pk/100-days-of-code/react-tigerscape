function Roars(){
    return(
        <section classs="section2">
        <div className="container">
          <h2 id="blogHead">Roars and Insights</h2>
          <div className="blogList">
            <article className="blog">
              <img
                src="https://plus.unsplash.com/premium_photo-1669725687150-15c603ac6a73?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Exploring Tiger Behavior: Insights from the Wild</h3>
              <p>
                Discover the enigmatic world of tiger behavior in its natural
                habitat. Our blog offers captivating insights into the daily lives
                and fascinating behaviors of these majestic creatures.
              </p>
            </article>
            <article className="blog">
              <img
                src="https://images.unsplash.com/photo-1591824438708-ce405f36ba3d?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Into the Jungle: Tales from TigerScape's Expeditions</h3>
              <p>
                Join us as we venture deep into the jungle, sharing captivating
                tales of discovery, adventure, and conservation efforts within
                TigerScape's vast wilderness.
              </p>
            </article>
            <article className="blog">
              <img
                src="https://images.unsplash.com/photo-1616869736810-a674a04d0440?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Wildlife Photography: Capturing Tigers in Their Element</h3>
              <p>
                Explore the artistry of wildlife photography as we showcase
                stunning images capturing tigers in their natural habitats,
                celebrating their beauty and grace.
              </p>
            </article>
            <article className="blog">
              <img
                src="https://plus.unsplash.com/premium_photo-1664304262892-e4b05b1d4951?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Conservation: Saving Tigers, One Story at a Time</h3>
              <p>
                Follow our journey through the Conservation Chronicles, where we
                document the tireless efforts and inspiring stories behind saving
                tigers, one step at a time.
              </p>
            </article>
            <article className="blog">
              <img
                src="https://images.unsplash.com/photo-1598214325784-4b28e27e64e9?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Tiger Tales: Stories of Survival and Resilience</h3>
              <p>
                Tiger Tales shares narratives of survival and resilience,
                unveiling the triumphs and challenges faced by these magnificent
                creatures in their ever-changing habitats.
              </p>
            </article>
            <article className="blog">
              <img
                src="https://images.unsplash.com/photo-1587219408895-24918d2b78af?q=80&w=1376&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt="images of tiger"
              />
              <h3>Tiger Spotting: Spotting Encounters and Observations</h3>
              <p>
                Dive into the thrill of Tiger Spotting, where we recount
                encounters and observations, capturing the essence of these
                elusive predators in their natural environment.
              </p>
            </article>
          </div>
        </div>
      </section>
    )
}
export default Roars