function Header(){
    return(
        <header>
        <div className="container">
        <span className="loraFont">TigerScape</span>
        <button id="menuButton">
            <span className="material-symbols-outlined" id="paw">
            pets
            </span>
        </button>
        <nav className="navClass">
            <ul className="navBar">
            <a href="#">
                <li>Home</li>
            </a>
            <a href="#">
                <li>Bookings</li>
            </a>
            <a href="#visitList">
                <li>Packages</li>
            </a>
            <a href="#">
                <li>About Us</li>
            </a>
            <a href="#">
                <li>Contact</li>
            </a>
            </ul>
        </nav>
        </div>
  </header>
    )
}

export default Header
