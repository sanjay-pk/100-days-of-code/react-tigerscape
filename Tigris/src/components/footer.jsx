function Footer(){
    return (
        <footer>
        <div className="container">
          <div className="footerHeader">
            <div>
              <h4>TigerScape</h4>
            </div>
            <div className="socialMedia">
              <a href="#">
                <i className="fa-brands fa-facebook" />
              </a>
              <a href="#">
                <i className="fa-brands fa-instagram" />
              </a>
              <a href="#">
                <i className="fa-brands fa-twitter" />
              </a>
            </div>
          </div>
          <div className="mainFooter">
            <nav className="navClass1">
              <ul className="navBar2">
                <a href="#">
                  <li>Terms and Conditions</li>
                </a>
                <a href="#">
                  <li>Booking &amp; Cancellation Policy</li>
                </a>
                <a href="#">
                  <li>Privacy Policy</li>
                </a>
                <a href="#">
                  <li>Contact Us</li>
                </a>
              </ul>
            </nav>
          </div>
        </div>
      </footer>
    )
}
export default Footer